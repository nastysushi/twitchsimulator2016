-- Configuration
function love.conf(t)
	t.title = "Twitch Simulator 2016"
	t.version = "0.10.0"
	t.window.width = 830
	t.window.height = 760

	--Windows users (Stop using windows)
	t.console = true
end
