debug = true
player = { x = 200, y = 710, speed = 600, img = nil }
-- "Bullets"
canShoot = true
canShootTimerMax = 0.2
canShootTimer = canShootTimerMax
bulletImg=nil
bullets={}
--Enemies
createEnemyTimerMax = 0.4
createEnemyTimer = createEnemyTimerMax
enemyImg=nil
enemies={}
--playerstats
isAlive = true
score = 0
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
function love.load(arg)
	player.img = love.graphics.newImage('assets/bat.png')
	bulletImg = love.graphics.newImage("assets/coin.png")
	enemyImg = love.graphics.newImage('assets/ghost.png')
	anotherEnemyImg = love.graphics.newImage('assets/spook.png')
	bg = love.graphics.newImage('assets/bg.png')
	doublespook = love.graphics.newImage("assets/doublespook.png")
end

function love.update(dt)
	canShootTimer = canShootTimer - (1 * dt)
	if canShootTimer < 0 then
		canShoot = true
	end
	if love.keyboard.isDown('escape') then
		love.event.push('quit')
	end
	if love.keyboard.isDown('left','a') then
		if player.x > 0 then
			player.x = player.x - (player.speed*dt)
		end
	elseif love.keyboard.isDown('right','d') then
		if player.x < (love.graphics.getWidth() - player.img:getWidth()) then
			player.x = player.x + (player.speed*dt) 
		end     
	end
	if love.keyboard.isDown(' ', 'rctrl', 'lctrl', 'ctrl') and canShoot and isAlive then
		newBullet = { x = player.x + (player.img:getWidth()/2), y = player.y, img = bulletImg }
		table.insert(bullets,newBullet)
		canShoot = false
		canShootTimer = canShootTimerMax
	end
	for i, bullet in ipairs(bullets) do
		bullet.y = bullet.y - (250 * dt)
		if bullet.y < 0 then
			table.remove(bullets,i)
		end
	end
	createEnemyTimer = createEnemyTimer - (1 * dt)
	if createEnemyTimer < 0 then
		createEnemyTimer = createEnemyTimerMax
		randomNumber=math.random(10,love.graphics.getWidth() - 10)
		newEnemy = { x = randomNumber, y = -10, img = enemyImg, speed=1 }
		anothernewEnemy = { x = randomNumber + 50, y= -10, img = anotherEnemyImg, speed=1}
		table.insert(enemies, anothernewEnemy)
		table.insert(enemies, newEnemy)
		end
	for i, enemy in ipairs(enemies) do
		enemy.speed = enemy.speed + 0.02
		enemy.y = enemy.y + (enemy.speed * 200 * dt)	
		if enemy.y > 850 then
			table.remove(enemies,i)
		end
	end
	for i, enemy in ipairs(enemies) do
	for j, bullet in ipairs(bullets) do
		if CheckCollision(enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight(), bullet.x, bullet.y, bullet.img:getWidth(), bullet.img:getHeight()) then
			table.remove(bullets, j)
			table.remove(enemies, i)
			score = score + 1
		end
	end
	if CheckCollision(enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight(), player.x, player.y, player.img:getWidth(), player.img:getHeight()) 
	and isAlive then
		table.remove(enemies, i)
		isAlive = false
	end
	end
	
	if not isAlive and love.keyboard.isDown('r') then
	-- remove all our bullets and enemies from screen
	bullets = {}
	enemies = {}

	-- reset timers
	canShootTimer = canShootTimerMax
	createEnemyTimer = createEnemyTimerMax

	-- move player back to default position
	player.x = 50
	player.y = 710

	-- reset our game state
	score = 0
	isAlive = true
	end
	if score > 20 then
		randomNumber=math.random(10,love.graphics.getWidth() - 10)
		newEnemy = { x = randomNumber, y = -10, img = doublespook, speed = 2 }
		table.insert(enemies, newEnemy)
		canShootTimerMax=0.05
	end
end

function love.draw(dt)
	love.graphics.draw(bg, 0, 0)
	if isAlive then
		love.graphics.draw(player.img, player.x, player.y)
	else
		love.graphics.print("Press 'R' to restart", love.graphics:getWidth()/2-50, 80)
	end
	for i, bullet in ipairs(bullets) do
		love.graphics.draw(bullet.img, bullet.x, bullet.y)
	end
	for i, enemy in ipairs(enemies) do
		love.graphics.draw(enemy.img, enemy.x, enemy.y)
	end
	love.graphics.print(score, love.graphics:getWidth()/2, 0)
	love.graphics.print("Followers:", (love.graphics:getWidth()/2)-100,0)
end
